<?php
include_once 'common.php';
include_once 'config.php';

$db_conn = NULL;

$tags_table = "tags";
$web_sites_table = "web_sites";
$tag_web_sites_table = "tag_web_sites";

class URLData
{
	public $url;
	public $url_desc;
}

function HandleErrorCode( $error )
{
	if ( $error->errno == 1062 )
	{
		echo "Value already exists.<br>";
	}			
	else
	{
		echo "Unknown error: " . $error->error . "(" . $error->errno . ")<br>";
	}
}

function ExecuteQuery( $query )
{
	//echo $query . "<br>";

	global $db_conn;
	$result = $db_conn->query($query);
	if ( $result == FALSE) 
	{
		HandleErrorCode( $db_conn );
	}
	
	return $result;
}
function ExecutePreparedQuery( $stmt )
{
	if (!$stmt->execute()) 
	{
		HandleErrorCode( $stmt );
	}
}

function ConnectToDB()
{
	global $db_conn;
	global $db_name;
	
	global $db_servername;
	global $db_username;
	global $db_password;

	if ( $db_conn == NULL )
	{
		$db_conn = new mysqli($db_servername, $db_username, $db_password );

		// Check connection
		if ($db_conn->connect_error) 
		{
			die("Connection failed: " . $db_conn->connect_error);
		} 	
	}
}

function SetUpDB()
{
	global $db_conn;
	global $db_name;

	ConnectToDB();
	$db_conn->select_db( $db_name );
}
function CloseDB()
{
	global $db_conn;
	if ( $db_conn != NULL )
	{
		$db_conn->close();
		$db_conn = NULL;
	}
}

function InitDB()
{
	global $db_conn;
	global $tags_table;
	global $web_sites_table;
	global $tag_web_sites_table;
	global $db_name;

	ConnectToDB();
	
	$create_db = "CREATE DATABASE $db_name";
	ExecuteQuery( $create_db );
	
	$db_conn->select_db( $db_name );

	$create_tags_query = 
	"CREATE TABLE $tags_table 
	(
	ID int NOT NULL AUTO_INCREMENT,
	TagName varchar(255) NOT NULL UNIQUE,
	PRIMARY KEY (ID)
	)
	";
	ExecuteQuery( $create_tags_query );
	
	$create_web_sites_query = 
	"CREATE TABLE $web_sites_table 
	(
	ID int NOT NULL AUTO_INCREMENT,
	UrlHash varchar(255) NOT NULL UNIQUE,
	Url text NOT NULL,
	Title text NOT NULL,
	PRIMARY KEY (ID)
	)
	";
	ExecuteQuery( $create_web_sites_query );
	
	$create_tag_web_site_query = 
	"CREATE TABLE $tag_web_sites_table
	(
	WebSiteID int NOT NULL,
	TagID int NOT NULL,
	
	CONSTRAINT UC_IDs UNIQUE (WebSiteID,TagID),
	FOREIGN KEY (WebSiteID) REFERENCES web_sites(ID) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (TagID) REFERENCES tags(ID) ON DELETE CASCADE ON UPDATE CASCADE
	)
	";
	ExecuteQuery( $create_tag_web_site_query );

	CloseDB();

	echo "<br>Successful init<br>";
}

function InsertNonExistingTags( $input_tags_array )
{
	global $db_conn;
	global $tags_table;

	$tags_array = array();
	$input_tags_count = count($input_tags_array);
	for ($i = 0; $i < $input_tags_count; ++$i)
	{
		$tag = FormatTag($input_tags_array[$i]);
		if ( ValidateTag($tag) )
		{
			$tags_array[] = $tag;
		}
	}

	$tags_count = count( $tags_array );
	
	$create_tmp_table_query = "CREATE TEMPORARY TABLE tags_tmp (TagName varchar(255))";
	ExecuteQuery($create_tmp_table_query );

	$insert_tmp_table_query = "INSERT INTO tags_tmp (TagName) VALUES ";
	$insert_tmp_table_query = $insert_tmp_table_query . "('$tags_array[0]')";
	for ( $i = 1; $i < $tags_count; ++$i )
	{
		$insert_tmp_table_query = $insert_tmp_table_query . ",('$tags_array[$i]')";
	}
	ExecuteQuery($insert_tmp_table_query);

	$insert_query = "INSERT INTO $tags_table (TagName) SELECT TagName FROM tags_tmp WHERE tags_tmp.TagName NOT IN (SELECT TagName FROM $tags_table)";
	ExecuteQuery($insert_query);

	$drop_tmp_table_query = "DROP TEMPORARY TABLE tags_tmp";
	ExecuteQuery($drop_tmp_table_query);

	if ( $tags_count > 0 )
	{
		$select_id_query = "SELECT ID FROM $tags_table WHERE TagName IN ('$tags_array[0]'";

		for ( $i = 1; $i < $tags_count; ++$i )
		{
			$select_id_query = $select_id_query . ",'$tags_array[$i]'";
		}

		$select_id_query = $select_id_query . ")";
		$select_result = ExecuteQuery( $select_id_query );
		$results_array = $select_result->fetch_all(MYSQLI_NUM);

		$tags_ids = array();
		$results_count = count($results_array);
		for ( $i = 0; $i < $results_count; ++$i)
		{
			$tags_ids[] = $results_array[$i][0];
		}

		$select_result->free();
		return $tags_ids;
	}

	return NULL;
}

function InsertUrlTitle( $url, $title )
{
	global $db_conn;
	global $web_sites_table;
	
	$hash = hash("md5", $url);

	$find_query = "SELECT ID FROM $web_sites_table WHERE UrlHash = '$hash'";
	$found_web_site = ExecuteQuery( $find_query );
	
	if ($found_web_site != NULL )
	{
		$web_site_id = $found_web_site->fetch_array(MYSQLI_NUM)[0];
		$found_web_site->free();
		if ( $web_site_id != NULL )
		{
			$update_query_prepared = $db_conn->prepare( 'UPDATE ' . $web_sites_table . ' SET Title = ? WHERE ID = ' . $web_site_id );
			$update_query_prepared->bind_param('s', $title);
			ExecutePreparedQuery( $update_query_prepared );

			$update_query_prepared->close();
			return $web_site_id;
		}
		
	}

	$insert_query_prepared = $db_conn->prepare("INSERT INTO " . $web_sites_table . " (UrlHash,Url,Title) VALUES (?,?,?)");
	$insert_query_prepared->bind_param('sss', $hash, $url, $title);
	ExecutePreparedQuery( $insert_query_prepared );

	$web_site_id = $insert_query_prepared->insert_id;
	$insert_query_prepared->close();
	
	return $web_site_id;
}
function ConnectTagsIDsToWebSite( $web_site_id, $tags_ids )
{
	global $tag_web_sites_table;

	$delete_query = "DELETE FROM $tag_web_sites_table WHERE WebSiteID = $web_site_id";
	ExecuteQuery( $delete_query );

	$tags_count = count( $tags_ids );
	if ( $tags_count != 0 )
	{		
		$insert_query = "INSERT INTO $tag_web_sites_table (WebSiteID,TagID) VALUES ";
		
		$insert_query = $insert_query . "($web_site_id," . $tags_ids[0] . ")";
		for ( $i = 1; $i < $tags_count; ++$i )
		{
			$insert_query = $insert_query . ",($web_site_id," . $tags_ids[$i] . ")";
		}
		ExecuteQuery( $insert_query );
	}
}

function GetTagsForWebSite( $web_site_id )
{
	global $tags_table;
	global $tag_web_sites_table;

	$select_tags_query = "SELECT TagName FROM $tags_table WHERE ID IN (SELECT TagID FROM $tag_web_sites_table WHERE WebSiteID = $web_site_id)";
	$select_tags_result = ExecuteQuery( $select_tags_query );

	$tags = "";

	if ( $select_tags_result == NULL )
	{
		$tags = '<p style="color:red;">EMPTY</p>';
	}
	else
	{
		$tags_array = $select_tags_result->fetch_all(MYSQLI_NUM);

		$tags_count = count( $tags_array );
		if ( $tags_count > 0 )
		{
			$tags = $tags_array[0][0];
			for ( $t = 1; $t < $tags_count; ++$t )
			{
				$tag = $tags_array[$t][0];
				$tags = $tags . ", $tag";
			}
		}
		$select_tags_result->free();
	}

	return $tags;
}

///////TESTS
function TestDB()
{
	SetUpDB();

	InsertTag("shadow");
	InsertTag("test");

	$tags = array("shadow","test");
	$url_id = InsertUrlTitle("http://cg.cs.uni-bonn.de/aigaion2root/attachments/MomentShadowMapping.pdf","Moment Shadow Mapping");
	ConnectTagsToWebSite( $url_id, $tags );

	CloseDB();
}

?>