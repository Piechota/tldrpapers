<?php
function ValidateTag( $tag )
{
	return $tag != "";
}
function FormatTag( $tag )
{
	return strtolower(trim($tag));
}
function TagsStrToArray( $tags_str )
{
    $tags_array = explode(",", $tags_str);
    for ( $i = 0; $i < count($tags_array); ++$i )
    {
        if ( ValidateTag( $tags_array[$i] ) )
        {
            $tags_array[$i] = FormatTag($tags_array[$i]);
        }
        else
        {
            unset($tags_array[$i]);
            $tags_array = array_values($tags_array);
            --$i;
        }
    }

    return $tags_array;
}
function TagsArrayToList( $tags_array )
{
    $tags_list = "";
    $tags_count = count( $tags_array );

    if ( $tags_count != 0 )
    {
        $tags_list = "('" . $tags_array[0] . "'";
        for ( $i = 1; $i < $tags_count; ++$i )
        {
            $tags_list = $tags_list . ",'" . $tags_array[$i] . "'";
        }
        $tags_list = $tags_list . ")";
    }

    return $tags_list;
}
function TagsQueryToList( $tags_query )
{
    $tags_list = "";
    $tags_count = count( $tags_query );

    if ( $tags_count != 0 )
    {
        $tags_list = "('" . $tags_query[0][0] . "'";
        for ( $i = 1; $i < $tags_count; ++$i )
        {
            $tags_list = $tags_list . ",'" . $tags_query[$i][0] . "'";
        }
        $tags_list = $tags_list . ")";
    }
    
    return $tags_list;
}

//https://stackoverflow.com/questions/4348912/get-title-of-website-via-link
//https://stackoverflow.com/a/26151993 - also next answer that is safer!!!!
function GetTitleFromWebSite($url)
{
    $ext = pathinfo($url, PATHINFO_EXTENSION );
    if ( $ext == 'pdf' )
    {
        return pathinfo($url, PATHINFO_FILENAME );
    }

    $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    );  

    $page = @file_get_contents($url, false, stream_context_create($arrContextOptions));
    if ( $page ) 
    {

        libxml_use_internal_errors( true );
        $dom=new DOMDocument;
        $dom->validateOnParse=false;
        $dom->standalone=true;
        $dom->preserveWhiteSpace=true;
        $dom->strictErrorChecking=false;
        $dom->recover=true;

        $dom->loadHTML( $page );
        libxml_clear_errors();


        $titles = $dom->getElementsByTagName( 'title' );
        if( $titles->length > 0 )
        {
            return $titles[0]->nodeValue;
        }
    }
    return "";
}
?>