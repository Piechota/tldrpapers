<?php
if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) 
{
	$uri = 'https://';
} 
else 
{
	$uri = 'http://';
}
$uri .= $_SERVER['HTTP_HOST'] . "/sites_view/";
header('Location: '.$uri); 
exit;
?>