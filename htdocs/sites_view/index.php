<!DOCTYPE HTML>  
<html>
<head>
<title>TLDR Papers</title>

<script>

function UpdateWebSites(tags)
{
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                document.getElementById("web_sites_table").innerHTML = this.responseText;
            }
        };
        
        xmlhttp.open("POST", "print_urls.php", true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        if ( tags != "")
        {
            xmlhttp.send("tags=" + tags);
        }
        else
        {
            xmlhttp.send();
        }
}

function UpdateTagsCombobox()
{
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                //document.getElementById("log").innerHTML =this.responseText;
                document.getElementById("tags_combobox").innerHTML = this.responseText;
            }
        };
        
        xmlhttp.open("POST", "tags_combobox.php", true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send();
}

function UpdateTagsTitle( web_site_url )
{
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                var json = this.responseText;
                //document.getElementById("log").innerHTML =json;
                if ( json != "" )
                {
                    var obj = JSON.parse(json);
                
                    document.getElementById("tags").value = obj.tags;
                    document.getElementById("title").value = obj.title;
                    UpdateWebSites( obj.tags );
                }
            }
        };
        xmlhttp.open("POST", "get_web_site.php", true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send( "url=" + web_site_url );
}

function RowClick(row)
{
    var url = row.cells[0].firstChild.getAttribute("href");
    document.getElementById("url").value = url;
    UpdateTagsTitle( url );
}

function AddWebSite() 
{
	var tags = document.getElementById("tags").value
    var title = document.getElementById("title").value
    var url = document.getElementById("url").value
    if (tags.length == 0 || url.length == 0 || title.length == 0 )
    {         
        return;
    } 
    else 
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                document.getElementById("log").innerHTML = this.responseText;
                UpdateWebSites(document.getElementById("tags").value);
                UpdateTagsCombobox();
            }
        };
        
        xmlhttp.open("POST", "add_url.php", true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send("tags=" + tags + "&title=" + title + "&url=" + url);
    }
}

function DeleteWebSite()
{
    var url = document.getElementById("url").value
    if (url.length == 0)
    {         
        return;
    } 
    else 
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                //document.getElementById("log").innerHTML = this.responseText;
                UpdateWebSites(document.getElementById("tags").value);
            }
        };
        
        xmlhttp.open("POST", "delete_url.php", true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send( "url=" + url );
    }
}

function DeleteTags()
{
    var tags = document.getElementById("tags").value
    if (tags.length == 0 || url.length == 0 || title.length == 0 )
    {         
        return;
    } 
    else 
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                //document.getElementById("log").innerHTML = this.responseText;
                document.getElementById("tags").value = "";
                UpdateWebSites(document.getElementById("tags").value);
                UpdateTagsCombobox();
            }
        };
        
        xmlhttp.open("POST", "delete_tags.php", true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send( "tags=" + tags );
    }
}

function AddTag( tag )
{
    var tags = document.getElementById("tags").value;
    if ( tags != "")
    {
        tags += "," + tag;
    }
    else
    {
        tags = tag;
    }
    document.getElementById("tags").value = tags;
    UpdateWebSites( tags );
}
</script>

<link rel="stylesheet" type="text/css" href="..\\common\\style.css">

</head>
<body>  
<?php include 'db_init.php'; ?>

<input type="text" id="tags" placeholder="tag0,tag1..." oninput="UpdateWebSites(this.value)">
<span><button type="button" class="buttonDanger" onclick="DeleteTags()">Delete tags</button></span>

<select id="tags_combobox" onchange="AddTag(this.value)">
<?php include 'tags_combobox.php'; ?>
</select>

<br>
<input type="text" id="title" placeholder="title">
<br>
<input type="text" id="url" oninput="UpdateTagsTitle(this.value)" placeholder="url">
<span>
    <button  onclick="AddWebSite()">Add/Update url</button>
    <button type="button" class="buttonDanger" onclick="DeleteWebSite()">Delete url</button>
</span>
<br>
<span>
    
    <a href="export_db.php" style="display: none;" id="exportFile"></a>
    <form action="import_db.php" id="importFileForm" method="post" enctype="multipart/form-data">
        <input type="file" name="importFile" id="importFile" onchange="document.getElementById('importFileForm').submit()" style="display: none;" />
    </form>
    <button type="button" onclick="document.getElementById('importFile').click();">Import</button>
    <button type="button" onclick="document.getElementById('exportFile').click();">Export</button>
</span>
<br><br>  
<div id="log"></div>

<table id="web_sites_table">
<?php include 'print_urls.php'; ?>
</table>
</body>
</html>