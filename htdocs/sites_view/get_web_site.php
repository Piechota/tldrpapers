<?php
include "../common/db_help.php";
global $tags_table;
global $web_sites_table;
global $tag_web_sites_table;

SetUpDB();

$url = $_POST["url"];
$hash = hash("md5", $url);

$select_web_site_id_query = "SELECT Title, ID FROM $web_sites_table WHERE UrlHash = '$hash'";
$select_web_site_id_result = ExecuteQuery( $select_web_site_id_query );

$web_site_data = NULL;
if ( $select_web_site_id_result )
{
    $web_site_data = $select_web_site_id_result->fetch_array(MYSQLI_NUM);
    $select_web_site_id_result->free();    
}

if ( $web_site_data != NULL )
{
    $json_data = new \stdClass();
    $json_data->title = $web_site_data[0];
    $json_data->tags = GetTagsForWebSite($web_site_data[1]);

    $json = json_encode($json_data);
    echo $json;
}
else
{
    $title = GetTitleFromWebSite($url);
    if ( $title != "" )
    {
        $json_data = new \stdClass();
        $json_data->title = $title;
        $json_data->tags = "";

        $json = json_encode($json_data);
        echo $json;
    }
    else
    {
        echo "";
    }
}

CloseDB();
?>
