<?php

include_once "../common/db_help.php";
global $tags_table;
global $web_sites_table;
global $tag_web_sites_table;

SetUpDB();

$tags_list = "";
$tags_count = 0;
$select_tags_query = "SELECT TagName FROM $tags_table ORDER BY TagName";
$select_tags_result = ExecuteQuery($select_tags_query);
$tags_query = $select_tags_result->fetch_all(MYSQLI_NUM);
$tags_count = count($tags_query);
$select_tags_result->free();

echo '<option disabled selected value>tags</option>';
for ( $i = 0; $i < $tags_count; ++$i )
{
    $tag = $tags_query[$i][0];
    echo '<option value="'. $tag . '">' . $tag . '</option>';
}

CloseDB();
?>