<?php
include_once "../common/db_help.php";
SetUpDB();

$tags_str = $_POST["tags"];
$title = $_POST["title"];
$url = $_POST["url"];

$tags_array = TagsStrToArray( $tags_str );
$tags_ids = InsertNonExistingTags( $tags_array );

$web_site_id = InsertUrlTitle($url, $title);
ConnectTagsIDsToWebSite( $web_site_id, $tags_ids );

CloseDB();
?>