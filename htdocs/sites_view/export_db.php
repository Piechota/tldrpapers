<?php

include_once '../common/db_help.php';

global $ftp_conn;

global $tags_table;
global $web_sites_table;
global $tag_web_sites_table;

$filename = "../ftp/db.csv";

$myfile = fopen($filename, "w");

SetUpDB();

{
    $select_query = "SELECT * FROM $tags_table";
    $select_result = ExecuteQuery($select_query);
    $query = $select_result->fetch_all(MYSQLI_NUM);
    $count = count($query);
    $select_result->free();

    $table = $count . "\n";
    for ( $i = 0; $i < $count; ++$i )
    {
        $table = $table . $query[$i][0] . ",'" . str_replace( "'", "''", $query[$i][1] ) . "'";
        $table = $table . "\n";    
    }

    fwrite($myfile, $table);
}

{
    $select_query = "SELECT * FROM $web_sites_table";
    $select_result = ExecuteQuery($select_query);
    $query = $select_result->fetch_all(MYSQLI_NUM);
    $count = count($query);
    $select_result->free();

    $table = $count . "\n";
    for ( $i = 0; $i < $count; ++$i )
    {
        $table = $table . $query[$i][0] . ",'" . str_replace( "'", "''", $query[$i][1] ) . "','" . str_replace( "'", "''", $query[$i][2] ) . "','" . str_replace( "'", "''", $query[$i][3] ) . "'";
        $table = $table . "\n";    
    }

    fwrite($myfile, $table);
}

{
    $select_query = "SELECT * FROM $tag_web_sites_table";
    $select_result = ExecuteQuery($select_query);
    $query = $select_result->fetch_all(MYSQLI_NUM);
    $count = count($query);
    $select_result->free();

    $table = $count . "\n";
    for ( $i = 0; $i < $count; ++$i )
    {
        $table = $table . $query[$i][0] . "," . $query[$i][1];
        $table = $table . "\n";    
    }

    fwrite($myfile, $table);
}

CloseDB();

fclose($myfile);

if(file_exists($filename)) {
    header('Content-Description: File Transfer');    
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    flush(); // Flush system output buffer
    readfile($filename);
    exit;
}
?>