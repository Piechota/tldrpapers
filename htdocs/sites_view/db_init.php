<?php
include_once "../common/db_help.php";
global $db_conn;
global $tags_table;
SetUpDB();

$query = "DESCRIBE " . $tags_table;

if ( $db_conn->query($query) == FALSE )
{
    InitDB();
}

CloseDB();
?>