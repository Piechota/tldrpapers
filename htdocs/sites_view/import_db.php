<?php

include_once '../common/db_help.php';

global $tags_table;
global $web_sites_table;
global $tag_web_sites_table;

$filename = "../ftp/db.csv";

if (move_uploaded_file($_FILES["importFile"]["tmp_name"], $filename) == FALSE) 
{
    echo "Sorry, there was an error uploading your file.";
    exit;
}

$myfile = fopen($filename, "r") or die("Unable to open file!");

SetUpDB();
//DELETE is slow but i am too lazy for TRUNCATE with FOREIGN KEY constraint
//search for:
//Cannot truncate a table referenced in a foreign key constraint
{
    $query = "DELETE FROM " . $tag_web_sites_table;
    ExecuteQuery($query);
}

{
    $query = "DELETE FROM " . $tags_table;
    ExecuteQuery($query);
}

{
    $query = "DELETE FROM " . $web_sites_table;
    ExecuteQuery($query);
}

{
    $query = "INSERT INTO " . $tags_table . "(ID, TagName) VALUES ";
    $count = filter_var(fgets($myfile), FILTER_SANITIZE_NUMBER_INT);
    $query = $query . "(" . fgets( $myfile ) . ")";
   // echo 'tags ' . $count . '<br>';
    for ( $i = 1; $i < $count; ++$i)
    {
        $query = $query . ",(" . fgets( $myfile ) . ")";
        //echo $i.'<br>';
    }
    //echo $query.'<br>';
    ExecuteQuery($query);
}

{
    $query = "INSERT INTO " . $web_sites_table . "(ID, UrlHash, Url, Title) VALUES ";
    $count = fgets($myfile);
    $query = $query . "(" . fgets( $myfile ) . ")";
    //echo 'web ' . $count . '<br>';
    for ( $i = 1; $i < $count; ++$i)
    {
        $query = $query . ",(" . fgets( $myfile ) . ")";
        //echo $i.'<br>';
    }
    //echo $query.'<br>';
    ExecuteQuery($query);
}

{
    $query = "INSERT INTO " . $tag_web_sites_table . "(WebSiteID, TagID) VALUES ";
    $count = fgets($myfile);
    $query = $query . "(" . fgets( $myfile ) . ")";
    //echo 'web_tag ' . $count . '<br>';
    for ( $i = 1; $i < $count; ++$i)
    {
        $query = $query . ",(" . fgets( $myfile ) . ")";
        //echo $i.'<br>';
    }
    //echo $query.'<br>';
    ExecuteQuery($query);
}

CloseDB();

fclose($myfile);

//This is really bad (but it gets job done :) )
if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) 
{
	$uri = 'https://';
} 
else 
{
	$uri = 'http://';
}
$uri .= $_SERVER['HTTP_HOST'] . "/sites_view/";
header('Location: '.$uri);
?>