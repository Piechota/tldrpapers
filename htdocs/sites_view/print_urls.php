<?php

include_once "../common/db_help.php";
global $tags_table;
global $web_sites_table;
global $tag_web_sites_table;

SetUpDB();

echo "<tr>";
echo "<th>Web Site</th>";
echo "<th>Tags</th>";
echo "</tr>";

$tags_list = "";
$tags_count = 0;
if ( isset($_POST["tags"]) && $_POST["tags"] != "" )
{
    $tags_array = TagsStrToArray( $_POST["tags"] );
    $tmp_tags_list = TagsArrayToList($tags_array);

    $select_tags_query = "SELECT TagName FROM $tags_table WHERE TagName IN $tmp_tags_list";
    $select_tags_result = ExecuteQuery($select_tags_query);

    $tags_query = $select_tags_result->fetch_all(MYSQLI_NUM);
    $tags_list = TagsQueryToList($tags_query);
    $tags_count = count($tags_query);
    $select_tags_result->free();
}

$select_web_sites_query = "";
if ( $tags_list == "" )
{
    $select_web_sites_query = "SELECT ID, Url, Title FROM $web_sites_table ORDER BY Title";    
}
else
{
    //http://web.archive.org/web/20150813211028/http://tagging.pui.ch/post/37027745720/tags-database-schemas
    $select_web_sites_query =   "SELECT webSites.ID, webSites.Url, webSites.Title
                                 FROM $tag_web_sites_table tagWebSite, $web_sites_table webSites, $tags_table tags
                                 WHERE tagWebSite.TagID = tags.ID
                                 AND (tags.TagName IN $tags_list)
                                 AND webSites.ID = tagWebSite.WebSiteID
                                 GROUP BY webSites.ID
                                 HAVING COUNT( webSites.ID )=$tags_count
                                 ORDER BY Title";
}

$select_web_sites_result = ExecuteQuery( $select_web_sites_query );

if ( $select_web_sites_result != NULL )
{
    $web_sites_array = $select_web_sites_result->fetch_all(MYSQLI_NUM);
    
    $web_sites_count = count( $web_sites_array);
    for ( $i = 0; $i < $web_sites_count; ++$i )
    {
        $web_site_data = $web_sites_array[$i];
        $web_site_tags = GetTagsForWebSite($web_site_data[0]);

        echo '<tr onclick="RowClick(this)">';
        echo '<td><a target="_blank" rel="noopener noreferrer" href="' . $web_site_data[1] . '">' . $web_site_data[2]. '</a></td>';
        echo '<td>' . $web_site_tags . '</td>';
        echo '</tr>';
    }

    $select_web_sites_result->free();
}

CloseDB();
?>